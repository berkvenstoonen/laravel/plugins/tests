<?php
declare(strict_types=1);

require_once 'src/Architecture.php';

$packages = [
    'laravel',
    'laravel-language-validator',
    'themes',
];

$includes = [];
foreach ($packages as $package) {
    $neonPath = realpath(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.$package.DIRECTORY_SEPARATOR.'phpstan.neon');

    if (is_file($neonPath)) {
        $includes[] = $neonPath;
    }
}

return [
    'includes'   => $includes,
];

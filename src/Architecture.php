<?php
declare(strict_types=1);

namespace BeTo\Tests;

use App\Models\Team;
use App\Models\User;
use BeTo\Laravel\Exceptions\Handler;
use BeTo\Laravel\Helpers\Build;
use Carbon\Carbon;
use Intervention\Image\Facades\Image;
use PHPat\Selector\Selector;
use PHPat\Selector\SelectorInterface;
use PHPat\Test\Builder\Rule;
use PHPat\Test\PHPat;
use Sentry\Serializer\SerializableInterface;
use Webmozart\Assert\Assert;

final readonly class Architecture
{
    public function test_laravel_sources(): Rule
    {
        return $this->getPackageSourcesRule('Laravel');
    }

    public function test_laravel_command_sources(): Rule
    {
        return $this->getCommandSourcesRule('Laravel');
    }

    public function test_laravel_language_validator_sources(): Rule
    {
        return $this->getPackageSourcesRule('LaravelLanguageValidator', ['Laravel']);
    }

    public function test_laravel_language_validator_command_sources(): Rule
    {
        return $this->getCommandSourcesRule('LaravelLanguageValidator', ['Laravel']);
    }

    public function test_themes_sources(): Rule
    {
        return $this->getPackageSourcesRule('Themes');
    }

    public function test_themes_command_sources(): Rule
    {
        return $this->getCommandSourcesRule('Themes');
    }

    public function test_error_handler_sources(): Rule
    {
        $rule = PHPat::rule();
        $rule
            ->classes(Selector::classname(Handler::class))
            ->canOnlyDependOn()
            ->classes(
                Selector::inNamespace('Sentry'),
                Selector::inNamespace('Symfony\Component\HttpKernel\Exception'),
                ...$this->getBaseAllowedClasses('Laravel'),
            )
            ->because('The error handler can only use base code, Sentry & Exceptions')
        ;
        return $rule;
    }

    public function test_test_sources(): Rule
    {
        $rule = PHPat::rule();
        $rule
            ->classes(Selector::inNamespace('BeTo\Tests'))
            ->canOnlyDependOn()
            ->classes(
                Selector::inNamespace('PHPat'),
                Selector::classname(Handler::class),
                Selector::classname(Build::class),
                ...$this->getBaseAllowedClasses('Tests'),
            )
            ->because('The library tests can only use basic code & PHPat')
        ;
        return $rule;
    }

    public function test_build_sources(): Rule
    {
        $rule = PHPat::rule();
        $rule
            ->classes(Selector::classname(Build::class))
            ->canOnlyDependOn()
            ->classes(
                Selector::inNamespace('Symfony\Component\Console'),
                Selector::inNamespace('Symfony\Component\Process'),
                ...$this->getBaseAllowedClasses('Laravel'),
            )
            ->because('The Build can only use base code & Symphony Command & Process')
        ;
        return $rule;
    }

    /**
     * @param list<string> $allowedPackages
     */
    private function getPackageSourcesRule(string $package, array $allowedPackages = []): Rule
    {
        $rule = PHPat::rule();
        $rule
            ->classes(Selector::inNamespace('BeTo\\'.$package))
            ->excluding(
                Selector::inNamespace('BeTo\\'.$package.'\\Console'),
                Selector::classname(Handler::class),
                Selector::classname(Build::class),
            )
            ->canOnlyDependOn()
            ->classes(
                ...$this->getBaseAllowedClasses($package, $allowedPackages),
            )
            ->because('The '.$package.' package can only use basic code')
        ;
        return $rule;
    }

    /**
     * @param list<string> $allowedPackages
     */
    private function getCommandSourcesRule(string $package, array $allowedPackages = []): Rule
    {
        $rule = PHPat::rule();
        $rule
            ->classes(
                Selector::inNamespace('BeTo\\'.$package.'\\Console'),
                // Selector::classname(Build::class),
            )
            ->canOnlyDependOn()
            ->classes(
                Selector::inNamespace('Symfony\Component\Console'),
                Selector::inNamespace('Symfony\Component\Process'),
                ...$this->getBaseAllowedClasses($package, $allowedPackages),
            )
            ->because('The '.$package.' library commands can only use base code & Symphony Command & Process')
        ;
        return $rule;
    }

    /**
     * @param list<string> $allowedPackages
     * @return list<SelectorInterface>
     */
    private function getBaseAllowedClasses(string $package, array $allowedPackages = []): array
    {
        $baseClasses = [
            Selector::classname(User::class),
            Selector::classname(Team::class),
            Selector::classname(Assert::class),
            Selector::classname(Carbon::class),
            Selector::classname(SerializableInterface::class),
            Selector::classname(Image::class),
            Selector::inNamespace('Illuminate'),
            Selector::inNamespace('Livewire'),
            Selector::inNamespace('BeTo\\'.$package),
            Selector::inNamespace('Symfony\Component\HttpFoundation'),
            Selector::AND(
                Selector::inNamespace('\\'),
                Selector::NOT(
                    Selector::inNamespace('/.+\\\\/', true),
                ),
            ),
        ];
        foreach ($allowedPackages as $allowedPackage) {
            $baseClasses[] = Selector::inNamespace('BeTo\\'.$allowedPackage);
        }
        return $baseClasses;
    }
}
